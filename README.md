# b1917

Läs Guds ord från din terminal. Innehåller Bibeln 1917 och de apokryfiska böckerna från 1921.

Forkat från [https://github.com/lukesmithxyz/kjv.git](https://github.com/lukesmithxyz/kjv.git) men texten ändrades till den svenska utgåvan.


## Användning

        användning: ./b1917 [flaggor] [hänvisning...]

        -l      tag fram boklistan
        -W      stäng av automatiskt radbyte
        -h      hjälp

        Hänvisningssätt:
        <Bok>
            Enskild bok
	    <Bok>:<Kapitel>
            Enskilt kapitel i en bok
	    <Bok>:<Kapitel>:<Vers>[,<Vers>]...
            Enskild(a) vers(er) i ett specifikt bokkapitel
	    <Bok>:<Kapitel>-<Kapitel>
            Ett kapitelomfång i en bok
	    <Bok>:<Kapitel>:<Vers>-<Vers>
            Ett versomfång i ett bokkapitel
	    <Bok>:<Kapitel>:<Vers>-<Kapitel>:<Vers>
            Ett kapitel- och versomfång i en bok
        
        /<Sök>
            Alla verser som stämmer överens med det inskrivna
        <Bok>/<Sök>
            Alla verser i en bok som stämmer överens med det inskrivna
        <Bok>:<Kapitel>/<Sök>
            Alla verser i ett bokkapitel som stämmer överens med det

## Bygg

b1917 kan byggas genom att klona repositoryn och sedan köra "make"-kommandot:

    git clone https://gitlab.com/agadroid/terminal-b1917.git
    cd terminal-b1917
    sudo make install

## Licens

Public domain