#!/bin/sh
# b1917: Läs Guds ord från din terminal
# License: Public domain

SELF="$0"

get_data() {
	sed '1,/^#EOF$/d' < "$SELF" | tar xzf - -O "$1"
}

if [ -z "$PAGER" ]; then
	if command -v less >/dev/null; then
		PAGER="less"
	else
		PAGER="cat"
	fi
fi

show_help() {
	exec >&2
	echo "användning: $(basename "$0") [flaggor] [hänvisning...]"
	echo
	echo "  -l      tag fram boklistan"
	echo "  -W      stäng av automatiskt radbyte"
	echo "  -h      hjälp"
	echo
	echo "  Hänvisningssätt:"
	echo "      <Bok>"
	echo "          Enskild bok"
	echo "      <Bok>:<Kapitel>"
	echo "          Enskilt kapitel i en bok"
	echo "      <Bok>:<Kapitel>:<Vers>[,<Vers>]..."
	echo "          Enskild(a) vers(er) i ett specifikt bokkapitel"
	echo "      <Bok>:<Kapitel>-<Kapitel>"
	echo "          Ett kapitelomfång i en bok"
	echo "      <Bok>:<Kapitel>:<Vers>-<Vers>"
	echo "          Ett versomfång i ett bokkapitel"
	echo "      <Bok>:<Kapitel>:<Vers>-<Kapitel>:<Vers>"
	echo "          Ett kapitel- och versomfång i en bok"
	echo
	echo "      /<Sök>"
	echo "          Alla verser som stämmer överens med det inskrivna"
	echo "      <Bok>/<Sök>"
	echo "          Alla verser i en bok som stämmer överens med det inskrivna"
	echo "      <Bok>:<Kapitel>/<Sök>"
	echo "          Alla verser i ett bokkapitel som stämmer överens med det inskrivna"
	exit 2
}

while [ $# -gt 0 ]; do
	isFlag=0
	firstChar="${1%"${1#?}"}"
	if [ "$firstChar" = "-" ]; then
		isFlag=1
	fi

	if [ "$1" = "--" ]; then
		shift
		break
	elif [ "$1" = "-l" ]; then
		# List all book names with their abbreviations
		get_data b1917.tsv | awk -v cmd=list "$(get_data b1917.awk)"
		exit
	elif [ "$1" = "-W" ]; then
		export KJV_NOLINEWRAP=1
		shift
	elif [ "$1" = "-h" ] || [ "$isFlag" -eq 1 ]; then
		show_help
	else
		break
	fi
done

cols=$(tput cols 2>/dev/null)
if [ $? -eq 0 ]; then
	export KJV_MAX_WIDTH="$cols"
fi

if [ $# -eq 0 ]; then
	if [ ! -t 0 ]; then
		show_help
	fi

	# Interactive mode
	while true; do
		printf "Bibeln 1917> "
		if ! read -r ref; then
			break
		fi
		get_data b1917.tsv | awk -v cmd=ref -v ref="$ref" "$(get_data b1917.awk)" | ${PAGER}
	done
	exit 0
fi

get_data b1917.tsv | awk -v cmd=ref -v ref="$*" "$(get_data b1917.awk)" | ${PAGER}
