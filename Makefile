PREFIX = /usr/local

b1917: b1917.sh b1917.awk b1917.tsv
	cat b1917.sh > $@
	echo 'exit 0' >> $@
	echo '#EOF' >> $@
	tar czf - b1917.awk b1917.tsv >> $@
	chmod +x $@

test: b1917.sh
	shellcheck -s sh b1917.sh

clean:
	rm -f b1917

install: b1917
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp -f b1917 $(DESTDIR)$(PREFIX)/bin
	chmod 755 $(DESTDIR)$(PREFIX)/bin/b1917

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/b1917

.PHONY: test clean install uninstall
